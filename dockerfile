
FROM postgres:12.10

ARG DEBIAN_FRONTEND=noninteractive
RUN apt update
RUN apt -y upgrade
RUN apt install wget -y

RUN mkdir /dumpFolder
RUN chmod 777 /dumpFolder/
WORKDIR /dumpFolder

COPY getFile.sh /dumpFolder/
COPY init.sh /docker-entrypoint-initdb.d/

RUN chmod +x /dumpFolder/getFile.sh
RUN chmod +x /docker-entrypoint-initdb.d/init.sh
