#!/bin/bash -x

chmod 777 /dumpFolder/
wget -P /dumpFolder/  http://dl.biocommons.org/uta/uta_20210129.pgd.gz

cat <<EOF >"$PGDATA/pg_hba.conf" 
# allow the anonymous user to access uta without password
# These lines must occur before more stringent authentication methods
host   all   anonymous     0.0.0.0/0      trust
host   all   uta_admin     0.0.0.0/0      trust
EOF

createuser --username "$POSTGRES_USER" anonymous
createuser --username "$POSTGRES_USER" uta_admin

createdb  --username "$POSTGRES_USER" -O uta_admin uta

gunzip -c /dumpFolder/uta_20210129.pgd.gz | psql uta
